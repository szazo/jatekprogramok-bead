export interface UpdateParams {
  time: number,
  index: fw.Index
}

export interface UpdateEvent {
  update (params: UpdateParams): void
}
