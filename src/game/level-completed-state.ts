import { LevelCompletedParams } from 'level'

export type LevelCompletedUiInfo = LevelCompletedParams & {
  isHighscore: boolean
}

export class LevelCompletedState {

  onContinueClicked: () => void
  onExitClicked: () => void

  constructor (private readonly results: LevelCompletedUiInfo) {
  }

  enter () {

    this.showHide('.level-completed', true)
    this.showMessage()
    this.showHighscore()
    this.showStars()

    this.continueButton().onclick = () => { this.onContinueClicked() }
    this.exitButton().onclick = () => { this.onExitClicked() }
  }

  exit () {
    this.showHide('.level-completed', false)
    this.continueButton().onclick = null
    this.exitButton().onclick = null
  }

  private showMessage () {
    let message: string

    if (this.results.stars === 1) {
      message = 'Good work!'
    } else if (this.results.stars === 2) {
      message = 'Nice work!'
    } else if (this.results.stars === 3) {
      message = 'Perfect! You are a hero!'
    }

    (document.querySelector('#level-completed-message')).textContent = message
  }

  private showHighscore () {
    const selector = '#level-completed-highscore'
    this.showHide(selector, this.results.isHighscore)

    if (this.results.isHighscore) {
      const div = document.querySelector(selector)
      div.textContent = `New Highscore: ${this.results.score}!`
    }
  }

  private showStars () {
    this.showHide('.star.star1', this.results.stars >= 1)
    this.showHide('.star.star2', this.results.stars >= 2)
    this.showHide('.star.star3', this.results.stars >= 3)
  }

  private continueButton (): HTMLButtonElement {
    return (document.querySelector('#completed-next-level'))
  }

  private exitButton (): HTMLButtonElement {
    return (document.querySelector('#completed-exit-level'))
  }

  private showHide (selector: string, show: boolean) {
    (<HTMLElement>(document.querySelector(selector))).style.display = show ? 'block' : 'none'
  }
}
