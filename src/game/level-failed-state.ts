export class LevelFailedState {

  onRetryClicked: () => void
  onExitClicked: () => void

  enter () {
    this.showHide('.level-failed', true)
    this.retryButton().onclick = () => { this.onRetryClicked() }
    this.exitButton().onclick = () => { this.onExitClicked() }
  }

  exit () {
    this.showHide('.level-failed', false)
    this.retryButton().onclick = null
    this.exitButton().onclick = null
  }

  private retryButton (): HTMLButtonElement {
    return (document.querySelector('#retry-level'))
  }

  private exitButton (): HTMLButtonElement {
    return (document.querySelector('#exit-level'))
  }

  private showHide (selector: string, show: boolean) {
    (<HTMLElement>document.querySelector(selector)).style.display = show ? 'block' : 'none'
  }
}
