import { Context } from './context'
import { LevelDescriptor } from 'level'
import { BasicLevel } from 'level'
import { StateMachine } from 'state-machine'
import { LevelWorld } from './level-world'
import { GameStorage } from 'storage'
import { LevelCompletedState, LevelCompletedUiInfo } from './level-completed-state'
import { LevelFailedState } from './level-failed-state'
import { AllLevelCompletedState } from './all-level-completed-state'
import { LevelState } from './level-state'

class FinishedState {}

export class Game {

  static readonly debug = false

  public onSucceeded: () => void
  public onExit: () => void

  private level: BasicLevel
  private levelWorld: LevelWorld
  private levelIndex: number
  private score: number

  private readonly sm: StateMachine

  constructor (private readonly canvas: HTMLCanvasElement,
              private readonly levels: LevelDescriptor[],
              private readonly gameStorage: GameStorage) {

    this.levelIndex = -1

    this.sm = new StateMachine()
  }

  continueGame () {

    const state = this.gameStorage.savedGame
    if (!state) {
      this.startNewGame()
    }

    this.score = state.score
    this.levelIndex = state.nextLevel

    this.tryStartLevel()
  }

  startNewGame () {

    this.levelIndex = 0
    this.score = 0

    this.gameStorage.clearSavedGame()
    this.tryStartLevel()
  }

  destroy () {
    this.sm.changeState(new FinishedState())
    this.destroyCurrentLevel()
  }

  private tryStartLevel () {
    if (this.levelIndex < this.levels.length) {

      this.startLevel(this.levelIndex, this.score)

    } else {

      this.allLevelCompleted()
    }
  }

  private startLevel (index: number, score: number) {

    this.destroyCurrentLevel()

    this.levelWorld = new LevelWorld(this.canvas)
    this.level = new BasicLevel(this.levels[index],
                               this.levelWorld.theContext(),
                               score)
    this.level.onLevelFailed = () => {
      this.level.onLevelFailed = null
      this.levelFailed()
    }
    this.level.onLevelCompleted = (params) => {

      const isHighscore = params.score > 0 && params.score > this.gameStorage.highScore

      if (isHighscore) {
        this.gameStorage.newHighscore(params.score)
      }

      this.gameStorage.saveGame({ nextLevel: index + 1, score: params.score })
      this.score = params.score

      this.level.onLevelCompleted = null
      this.levelCompleted({ ...params, isHighscore })
    }

    const state = new LevelState(this.level, this.levelWorld)
    this.sm.changeState(state)
  }

  private levelCompleted (results: LevelCompletedUiInfo) {

    const state = new LevelCompletedState(results)
    state.onContinueClicked = () => {
      this.levelIndex++
      this.tryStartLevel()
    }
    state.onExitClicked = () => {
      this.finished()
    }

    this.sm.changeState(state)
  }

  private levelFailed () {

    const state = new LevelFailedState()
    state.onRetryClicked = () => {
      this.startLevel(this.levelIndex, this.score)
    }
    state.onExitClicked = () => {
      this.finished()
    }
    this.sm.changeState(state)
  }

  private allLevelCompleted () {
    const state = new AllLevelCompletedState()
    state.onExitClicked = () => {
      this.finished()
    }

    this.sm.changeState(state)
  }

  private finished () {
    this.sm.changeState(new FinishedState())

    this.onExit()
  }

  private destroyCurrentLevel () {
    if (this.level && this.levelWorld) {
      this.level.destroy()
      this.levelWorld.destroy()
    }
  }
}
