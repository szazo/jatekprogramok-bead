import { Context } from './context'

interface Level {
  images: fw.Image[]
  update (time: number): void
  draw (ctx: CanvasRenderingContext2D): void
}

export class LevelWorld {

  static debug = false

  private ctx: CanvasRenderingContext2D
  private world: planck.World
  private scene: fw.Scene
  private index: fw.Index
  private context: Context

  private isPaused: boolean
  private frameId: number
  private level: Level

  constructor (private readonly canvas: HTMLCanvasElement) {

    this.initializeWorld()
  }

  initializeWorld () {
    this.ctx = this.canvas.getContext('2d')
    this.ctx.scale(1.5, 1.5)

    this.world = new planck.World(new planck.Vec2(0, 0))
    this.world.on('pre-solve', (contact) => this.handleWorldPreSolve(contact))

    this.scene = new fw.Scene()

    this.context = new Context(this.scene, this.world)
  }

  start (level: Level) {

    this.level = level

    fw.load(level.images, this.startLoop.bind(this))

    const self = this
    document.onkeypress = function (e) {
      self.scene.fire('keyPressed', e.keyCode)
    }
  }

  pause () {
    this.isPaused = true
  }

  destroy () {
    this.ctx.resetTransform() // reset the scale
    document.onkeypress = null
    if (this.frameId) {
      cancelAnimationFrame(this.frameId)
    }
  }

  public theContext () {
    return this.context
  }

  private handleWorldPreSolve (contact) {
    const bodyA = contact.getFixtureA().getBody()
    const bodyB = contact.getFixtureB().getBody()

    const entityA = bodyA.getUserData()
    const entityB = bodyB.getUserData()

    this.scene.fireToEntity(entityA, 'contact', { body: bodyB, entity: entityB })
    this.scene.fireToEntity(entityB, 'contact', { body: bodyA, entity: entityA })
  }

  private loop (time) {

    if (this.isPaused) {
      return
    }

    this.update(time)
    this.draw()

    this.frameId = requestAnimationFrame(this.loop.bind(this))
  }

  private startLoop () {
    this.frameId = requestAnimationFrame(this.loop.bind(this))
  }

  private update (time: number) {

    this.world.step(1 / 60, 5, 5)
    this.executeTasks()

    this.index = fw.createIndex(this.scene, 64)
    this.scene.fire('update', { time, index: this.index })
    this.level.update(time)
  }

  private draw () {
    // this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
    // background
    this.ctx.fillStyle = '#FFE8C9'
    this.ctx.fillRect(0, 0, 640, 480)

    this.scene.fire('draw', this.ctx)
    this.level.draw(this.ctx)

    if (LevelWorld.debug) {
      this.indexDebug()
    }
  }

  private indexDebug () {
    for (const entity of this.scene.entities) {

      this.ctx.beginPath()
      this.ctx.lineWidth = 1
      this.ctx.strokeStyle = 'red'
      this.ctx.rect(entity.getLeft(), entity.getTop(), entity.getWidth(), entity.getHeight())
      this.ctx.stroke()
    }
  }

  private executeTasks () {

    while (this.context.hasTask) {
      const task = this.context.dequeue()
      task()
    }
  }
}
