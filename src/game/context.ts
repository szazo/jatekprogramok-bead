export class Context {

  private tasks: any[]

  constructor (public readonly scene,
              public readonly world: planck.World) {

    this.tasks = []
  }

  runLater (tasks) {
    this.tasks = this.tasks.concat(tasks)
  }

  get hasTask () {
    return this.tasks.length > 0
  }

  dequeue () {
    const task = this.tasks.shift()

    return task
  }
}
