import { BasicLevel } from 'level'
import { LevelWorld } from './level-world'

export class LevelState {

  constructor (
    private readonly level: BasicLevel,
    private readonly levelWorld: LevelWorld) {
  }

  enter () {
    this.levelWorld.start(this.level)
  }

  exit () {
    this.levelWorld.pause()
  }
}
