export class AllLevelCompletedState {

  onExitClicked: () => void

  constructor () {
  }

  enter () {

    this.showHide('.all-level-completed', true)

    this.exitButton().onclick = () => { this.onExitClicked() }
  }

  exit () {
    this.showHide('.all-level-completed', false)
    this.exitButton().onclick = null
  }

  private exitButton (): HTMLButtonElement {
    return (document.querySelector('#all-level-completed-exit'))
  }

  private showHide (selector: string, show: boolean) {
    (<HTMLElement>(document.querySelector(selector))).style.display = show ? 'block' : 'none'
  }
}
