
interface SavedGame {
  score: number
  nextLevel: number
}

interface State {
  highscore: number
  savedGame: SavedGame | null
}

const KEY = 'cpm-game'

export class GameStorage {

  get hasHighscore () {
    return this.highScore > 0
  }

  get highScore () {
    return this.currentState().highscore
  }

  newHighscore (score: number) {
    this.updateState((state) => {
      state.highscore = score
    })
  }

  clearSavedGame () {
    this.updateState((state) => {
      state.savedGame = null
    })
  }

  saveGame (game: SavedGame) {
    this.updateState((state) => {
      state.savedGame = {
        nextLevel: game.nextLevel,
        score: game.score
      }
    })
  }

  get hasSavedGame () {
    return !!this.savedGame
  }

  get savedGame (): SavedGame | null {
    return this.currentState().savedGame
  }

  private updateState (updater: (state: State) => void) {
    const state = this.currentState()
    updater(state)

    localStorage.setItem(KEY, JSON.stringify(state))
  }

  private currentState (): State {
    const item = localStorage.getItem(KEY)
    if (!item) {
      return this.defaultState()
    }

    try {
      return JSON.parse(item) as State
    } catch (err) {
      return this.defaultState()
    }
  }

  private defaultState (): State {
    return {
      highscore: 0,
      savedGame: null
    }
  }
}
