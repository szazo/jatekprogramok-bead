export class Coord {
  constructor (
    public readonly x: number,
    public readonly y: number) {

  }

  static fromPosition (position: { x: number, y: number }) {
    return new Coord(position.x, position.y)
  }

  d (other: Coord) {
    return Math.sqrt((other.x - this.x) ** 2 + (other.y - this.y) ** 2)
  }

  toString () {
    return this.x + '#' + this.y
  }

  clone () {
    return new Coord(this.x, this.y)
  }

  equals (other: Coord) {
    return this.x === other.x && this.y === other.y
  }
}
