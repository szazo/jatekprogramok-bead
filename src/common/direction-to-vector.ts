import { Direction } from './direction'

export class DirectionToVector {

  static calculate(direction: Direction) {

    let x = 0
    let y = 0
    
    switch (direction) {
      case Direction.left:
        x = -1
        break
      case Direction.right:
        x = 1
        break
      case Direction.up:
        y = -1
        break
      case Direction.down:
        y = 1
        break
      case Direction.upLeft:
        x = -1
        y = -1
        break
      case Direction.upRight:
        x = 1
        y = -1
        break
      case Direction.downLeft:
        x = -1
        y = 1
        break
      case Direction.downRight:
        x = 1
        y = 1
        break
    }

    return { x: x, y: y }
  }
}
