import { Coord } from './coord'

interface QueueNode {
  score: number
}

// NOTE: not the best data structure for priority queue
class MinQueue<T extends QueueNode> {

  private readonly entries: T[] = []

  enqueue (value: T) {
    this.entries.push(value)
  }

  dequeue (): T {
    if (this.entries.length === 0) {
      throw new Error('Empty queue')
    }

    return this.entries.shift()
  }

  public resort () {
    this.entries.sort((a, b) => a.score - b.score)
  }

  get size () {
    return this.entries.length
  }
}

enum Color {
  white,
  grey, // added to the queue
  black // dequeued
}

class GraphNode<T> {

  parent: GraphNode<T> = null
  fScore: number = Number.MAX_VALUE
  gScore: number = Number.MAX_VALUE
  color: Color = Color.white

  constructor (public readonly key: T) {
  }

  get score () {
    return this.fScore
  }
}

export interface NodeProvider {
  provide (coord: Coord)
}

type SearchNode = GraphNode<Coord>

export class PathFinder {

  private readonly openSet = new MinQueue<SearchNode>()
  private readonly nodes = new Map<string, SearchNode>()

  constructor (private readonly nodeProvider: NodeProvider) {

  }

  calculate (start: Coord, goal: Coord): Coord[] {

    const startNode = new GraphNode(start)
    startNode.gScore = 0
    startNode.fScore = this.heuristic(start, goal)
    startNode.color = Color.grey

    this.openSet.enqueue(startNode)

    const length = 0

    while (this.openSet.size > 0) {
      const u = this.openSet.dequeue()
      if (u.key.d(goal) < 10) {
        const path = this.constructPath(u)
        return path
      }

      u.color = Color.black

      for (const v of this.neighbours(u)) {
        if (v.color === Color.black) {
          continue
        }

        const newG = u.gScore + (u.key.d(v.key))

        if (v.color === Color.white) {
          // this is a new node
          v.color = Color.grey
          this.openSet.enqueue(v)
        } else if (newG >= v.gScore) {
          // score not better
          continue
        }

        v.parent = u
        v.gScore = newG
        v.fScore = newG + this.heuristic(v.key, goal)
      }

      this.openSet.resort()
    }

    return []
  }

  private constructPath (node: SearchNode) {
    const path = []
    do {
      path.push(node.key)
      node = node.parent
    } while (node)

    path.reverse()
    return path
  }

  private node (coord: Coord) {
    let node = this.nodes.get(coord.toString())
    if (!node) {
      node = new GraphNode<Coord>(coord)
      this.nodes.set(coord.toString(), node)
    }

    return node
  }

  private neighbours (node: GraphNode<Coord>) {

    const coords = this.nodeProvider.provide(node.key)
    return coords.map((x) => this.node(x))
  }

  private heuristic (current: Coord, goal: Coord) {
    return current.d(goal)
  }
}
