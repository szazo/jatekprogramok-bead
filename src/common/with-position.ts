import { Position } from './position'

export interface WithPosition {
  position: Position
}
