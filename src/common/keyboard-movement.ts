import { Direction } from './direction'

export class KeyboardMovement {

  direction (): Direction {

    const vector = this.vector()
    return this.toDirection(vector)
  }

  vector() {
    let x = 0
    let y = 0

    if (fw.isDown(40)) {
      y += 1
    }
    if (fw.isDown(38)) {
      y -= 1
    }
    if (fw.isDown(37)) {
      x -= 1
    }
    if (fw.isDown(39)) {
      x += 1
    }

    return { x: x, y: y }
  }

  private toDirection(vector: { x: number, y: number }) {
    const x = vector.x
    const y = vector.y
    if (x < 0) {
      if (y < 0) {
        return Direction.upLeft
      } else if (y > 0) {
        return Direction.downLeft
      } else {
        return Direction.left
      }
    } else if (x > 0) {
      if (y < 0) {
        return Direction.upRight
      } else if (y > 0) {
        return Direction.downRight
      } else {
        return Direction.right
      }
    } else {
      if (y < 0) {
        return Direction.up
      } else if (y > 0) {
        return Direction.down
      }
    }

    return Direction.none
  }
}
