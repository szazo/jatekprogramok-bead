/**
 * Calculates required distance to go with the specified speed and passed time.
 */
export class DistanceCalculator {

  private lastTime: number

  constructor (private readonly speedTilePerSec: number) {

  }

  advance (time: number) {

    if (!this.lastTime) {
      this.lastTime = time
      return 0
    }

    const SPEED_TILE_PER_S = (32 / 1000)
    const speed = this.speedTilePerSec * SPEED_TILE_PER_S

    // calculate the distance to go
    const diff = time - this.lastTime
    const distanceToGo = speed * diff

    this.lastTime = time

    return distanceToGo
  }
}
