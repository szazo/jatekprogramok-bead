interface IntervalTimerOptions {
  fireOnFirst: boolean
}

export class IntervalTimer {

  private lastTime = -1

  constructor (private readonly interval: number,
              private readonly options: IntervalTimerOptions) {
  }

  reset (time: number) {
    this.lastTime = time
  }

  check (time: number) {

    if (this.lastTime === -1) {
      this.lastTime = time
      return this.options.fireOnFirst
    }

    const elapsed = time - this.lastTime

    if (elapsed > this.interval) {
      this.lastTime = time - (elapsed % this.interval)
      return true
    }

    return false
  }
}
