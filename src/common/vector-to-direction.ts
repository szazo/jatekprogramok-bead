import { Direction } from './direction'
import { Position } from './position'

export class VectorToDirection {

  static calculate(current: Position, target: Position) {
    const dy = target.y - current.y
    const dx = target.x - current.x

    if (dx === 0) {
      if (dy === 0) {
        return Direction.none
      } else if (dy < 0) {
        return Direction.up
      } else if (dy > 0) {
        return Direction.down
      }
    }

    const tan = Math.abs(dy / dx)
    const rad = Math.atan(tan)

    const piPer6 = Math.PI / 6
    const piPer2 = Math.PI / 2
    if (rad > piPer2 - piPer6) {
      // up - down
      if (dy < 0) {
        return Direction.up
      } else {
        return Direction.down
      }
    } else if (rad < piPer6) {
      // left - right
      if (dx > 0) {
        return Direction.right
      } else {
        return Direction.left
      }
    } else {
      // diagonal
      if (dx > 0) {
        if (dy < 0) {
          return Direction.upRight
        } else {
          return Direction.downRight
        }
      } else {
        if (dy < 0) {
          return Direction.upLeft
        } else {
          return Direction.downLeft
        }
      }
    }
  }
}
