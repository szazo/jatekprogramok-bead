import { Coord } from './coord'
import { Position } from './position'

/**
 * Advances the specified distance on the path
 */
export class PathAdvancer {
  constructor (public readonly currentPath: Coord[]) {
  }

  advance (currentPosition: Position, distanceToGo: number): Position | false {

    if (this.isReached) {
      return false
    }

    let coord = new Coord(currentPosition.x, currentPosition.y)

    do {
        const next = this.currentPath[0]
        const stepDistance = next.d(coord)
        if (stepDistance < distanceToGo) {
          coord = next
          this.currentPath.shift()
          distanceToGo -= stepDistance
        } else if (distanceToGo < stepDistance) {
          // we need to go on path the specified distance
          const dx = next.x - coord.x
          const dy = next.y - coord.y
          const normx = 1 / stepDistance * dx
          const normy = 1 / stepDistance * dy
          const nx = coord.x + normx * distanceToGo
          const ny = coord.y + normy * distanceToGo

          coord = new Coord(nx, ny)
          distanceToGo = 0
        }
      } while (distanceToGo > 0 && this.currentPath.length > 0)
    return coord
  }

  get isReached () {
    return this.currentPath.length === 0
  }
}
