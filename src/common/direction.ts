export enum Direction {
  none = 'none',
  left = 'left',
  right = 'right',
  up = 'up',
  down = 'down',
  upLeft = 'upLeft',
  upRight = 'upRight',
  downLeft = 'downLeft',
  downRight = 'downRight'
}

