import { Position } from './position'

export class VectorMovement {

  static calculate(vector: Position, distanceToGo: number) {
    const magnitude = Math.sqrt(vector.x ** 2 + vector.y ** 2)
    const normx = vector.x / magnitude
    const normy = vector.y / magnitude
    return { x: normx * distanceToGo, y: normy * distanceToGo }
  }
}
