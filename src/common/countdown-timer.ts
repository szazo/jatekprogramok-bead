export class CountdownTimer {

  private lastTime = -1

  constructor (private remainingTime: number) {
  }

  check (time: number) {

    if (this.lastTime === -1) {
      this.lastTime = time
      return false
    }

    const diff = time - this.lastTime
    this.lastTime = time
    this.remainingTime -= diff

    if (this.remainingTime < 0) {
      this.remainingTime = 0
    }

    return this.expired
  }

  get expired () {
    return this.remainingTime <= 0
  }

  get remaining () {
    return this.remainingTime
  }
}
