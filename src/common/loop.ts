export abstract class Loop {

  private stopped: boolean

  constructor () {
    this.stopped = false
  }

  start () {
    this.startLoop()
  }

  stop () {
    this.stopped = true
  }

  startLoop () {
    requestAnimationFrame(this.loop.bind(this))
  }

  loop (time: number) {

    if (this.stopped) {
      return
    }

    this.update(time)
    requestAnimationFrame(this.loop.bind(this))
  }

  protected abstract update (time: number): void
}
