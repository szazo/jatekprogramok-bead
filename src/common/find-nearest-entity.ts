import { Coord } from './coord'
import { Position } from './position'
import { WithPosition } from './with-position'

export function findNearest<T extends WithPosition> (pos: Position, entities: T[]) {
  if (entities.length === 0) {
    return null
  }

  const coord = Coord.fromPosition(pos)
  let minEntity = entities[0]
  let minD = Coord.fromPosition(minEntity.position).d(coord)
  for (let i = 1; i < entities.length; i++) {

    const current = entities[i]
    const d = Coord.fromPosition(current.position).d(coord)
    if (d < minD) {
      minEntity = current
      minD = d
    }
  }

  return minEntity
}
