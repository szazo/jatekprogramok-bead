export enum EntityCategories {
  none = 0,
  wall = 1,
  cat = 2,
  mouse = 4,
  bullet = 8
}
