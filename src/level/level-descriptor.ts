export const TILE_SIZE = 32

export const W = 1 // wall
export const M = 2 // mouse
export const G = 3 // cat alias hero
export const C = 4 // cheese
export const H = 5 // hole

export type Tile = 0 | 1 | 2 | 3 | 4 | 5
export type LevelMap = Tile[][]
export type LevelDescriptor = {
  map: LevelMap,
  gameSeconds: number,
  // mouseSpawn: {
  //   intervalSeconds: { min: number, max: number }
  // }
  mouseIntervalMs: {
    min: number
    max: number
  }
  bulletCount: number
}
