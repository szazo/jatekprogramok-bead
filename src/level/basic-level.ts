import { Cat } from 'cat'
import { LevelDescriptor, TILE_SIZE } from './level-descriptor'
import { Mouse } from 'mouse/mouse'
import { Hole, Wall, Cheese } from 'static-entities'
import { Context } from 'game'
import { Bullet } from 'cat'
import { LevelInfo } from './level-info'
import { CountdownTimer, IntervalTimer } from 'common'
import { FireParams } from 'cat/cat-fire';

const MOUSE_KILL_SCORE = 10
const TEXT_COLOR = '#f8ff30'

export interface LevelCompletedParams {
  stars: 1 | 2 | 3
  score: number
}

export class BasicLevel implements LevelInfo {

  get cat () {
    return this.theCat
  }

  get cheeses () {
    return this.cheeseList
  }

  get holes () {
    return this.holeList
  }

  // all of the required image for the level
  images = [ Cat.sprite, Wall.sprite, Mouse.sprite, Cheese.sprite ]

  onLevelFailed: () => void
  onLevelCompleted: (params: LevelCompletedParams) => void

  private readonly entities: fw.Entity[]
  private readonly mouses: Set<Mouse>
  private readonly cheeseList: Cheese[]
  private readonly holeList: Hole[]
  private theCat: Cat
  private descriptor: LevelDescriptor
  private spawnTimer: IntervalTimer
  private gameTimer: CountdownTimer
  private remainingCheeseCount: number
  private remainingBullets: number

  constructor (level: LevelDescriptor,
              private readonly context: Context,
              private score: number) {

    this.entities = []
    this.mouses = new Set<Mouse>()
    this.cheeseList = []
    this.holeList = []

    this.initLevel(level)
  }

  drawWall (startX: number, startY: number, direction: { x: number, y: number }, length: number) {

    let x = startX
    let y = startY

    for (let i = 0; i < length; i++) {
      this.add(new Wall(x * TILE_SIZE, y * TILE_SIZE, this.context))

      x += direction.x
      y += direction.y
    }
  }

  destroy () {
    for (const entity of this.entities) {
      this.removeLater(entity)
    }
  }

  update (time: number) {

    this.gameTimer.check(time)
    if (this.checkLevelFinished()) {
      return
    }

    if (this.holes.length === 0) {
      return
    }

    if (this.spawnTimer.check(time) && this.cheeseList.length > 0) {

      // if we active have cheese
      const index = Math.floor(Math.random() * this.holes.length)
      const hole = this.holes[index]

      this.addMouse(hole.x, hole.y + 5)
    }
  }

  draw (ctx: CanvasRenderingContext2D) {

    ctx.save()

    // point
    ctx.globalAlpha = 0.6
    ctx.fillStyle = '#d89d36'
    ctx.fillRect(4, 4, 632, 24)

    // ctx.shadowBlur = 20
    // ctx.shadowColor = 'black'
    // ctx.fillRect(8, 8, 624, 16)

    this.drawTime(ctx)
    this.drawScore(ctx)
    this.drawRemainingBullets(ctx)

    ctx.restore()
  }

  addCheese (cheese: Cheese) {
    this.add(cheese)
  }

  removeCheese (cheese: Cheese) {
    this.removeFromArray(this.cheeseList, cheese)
    this.remove(cheese)
  }

  private addHole (x: number, y: number) {
    const hole = new Hole(x, y)
    this.holeList.push(hole)
    this.add(hole)
  }

  handleCatFire (params: FireParams) {

    if (this.remainingBullets <= 0) {
      return
    }

    const bullet = new Bullet(params.x, params.y, params.direction, this.context)
    bullet.onVanish = () => {
      bullet.onVanish = null
      this.removeLater(bullet)
    }
    this.remainingBullets--
    this.add(bullet)
  }

  addEntities (entities: fw.Entity[]) {
    for (const entity of entities) {
      this.add(entity)
    }
  }

  add (entity: fw.Entity) {

    if (entity instanceof Cheese) {
      this.cheeseList.push(entity)
    }

    this.entities.push(entity)
    this.context.scene.add(entity)
  }

  public remove (entity: fw.Entity) {
    this.removeFromArray(this.entities, entity)
    this.context.scene.remove(entity)
  }

  private initLevel (level: LevelDescriptor) {

    this.descriptor = level
    this.gameTimer = new CountdownTimer(level.gameSeconds * 1000)
    this.startNextSpawnTimer()
    this.remainingBullets = level.bulletCount

    this.drawMainWalls()
    let i = 0
    for (const row of level.map) {
      let j = 0
      for (const col of row) {
        const x = (j + 1) * TILE_SIZE
        const y = (i + 1) * TILE_SIZE

        switch (col) {
          case 1:
            this.add(new Wall(x, y, this.context))
            break
          case 2:
            this.addMouse(x, y)
            break
          case 3:
            this.addCat(x, y)
            break
          case 4:
            this.createCheese(x, y)
            break
          case 5:
            this.addHole(x, y)
            break
        }

        j++
      }
      i++
    }

    this.remainingCheeseCount = this.cheeseList.length
  }

  private startNextSpawnTimer () {

    const min = this.descriptor.mouseIntervalMs.min
    const max = this.descriptor.mouseIntervalMs.max
    const interval = Math.floor(Math.random() * max) + min

    this.spawnTimer = new IntervalTimer(interval, { fireOnFirst: false })
  }

  private drawMainWalls () {
    this.drawWall(0, 0, { x: 1, y: 0 }, 20)
    this.drawWall(0, 1, { x: 0, y: 1 }, 13)
    this.drawWall(0, 14, { x: 1, y: 0 }, 20)
    this.drawWall(19, 1, { x: 0, y: 1 }, 13)
  }

  private drawTime (ctx: CanvasRenderingContext2D) {

    const color = this.remainingSeconds() < 10 ? '#6bffd8' : TEXT_COLOR

    ctx.font = '12px Arial'
    ctx.fillStyle = color
    ctx.fillText('Remaining time: ' + this.remainingTimeString(), 20, 20)
  }

  private remainingTimeString () {
    const date = new Date(null)
    date.setSeconds(this.remainingSeconds())
    return date.toISOString().substr(11, 8)
  }

  private remainingSeconds() {
    return Math.floor(this.gameTimer.remaining / 1000)
  }

  private drawScore (ctx: CanvasRenderingContext2D) {
    ctx.font = '12px Arial'
    ctx.fillStyle = TEXT_COLOR
    const scoreStr = this.score.toString()
    ctx.fillText('Score: ' + this.padStart(scoreStr, 4), 300, 20)
  }

  private drawRemainingBullets (ctx: CanvasRenderingContext2D) {

    const color = this.remainingBullets < 20 ? '#ff1403' : TEXT_COLOR
    
    ctx.font = '12px Arial'
    ctx.fillStyle = color
    const bulletsStr = this.remainingBullets.toString()
    ctx.fillText('Wool ball: ' + this.padStart(bulletsStr, 5), 500, 20)
  }

  private padStart (str: string, length: number) {
    while (str.length < length) {
      str = '0' + str
    }

    return str
  }

  private addMouse (x: number, y: number) {
    const mouse = new Mouse(x, y, this, this.context)
    mouse.onDead = () => {
      if (this.mouses.has(mouse)) {
        this.mouses.delete(mouse)
        this.removeLater(mouse)
        this.increaseScore(MOUSE_KILL_SCORE)
        this.checkLevelFinished()
      }
    }
    mouse.onHidden = (withCheese: boolean) => {
      if (this.mouses.has(mouse)) {
        this.mouses.delete(mouse)
        this.removeLater(mouse)
        if (withCheese) {
          this.remainingCheeseCount--
        }
        this.checkLevelFinished()
      }
    }
    this.add(mouse)
    this.mouses.add(mouse)
  }

  private increaseScore (score: number) {
    this.score += score
  }

  private checkLevelFinished () {

    if (this.isLevelFailed()) {
      this.context.runLater(() => {
        if (this.onLevelFailed) {
          this.onLevelFailed()
        }
      })

      return true
    }

    if (this.isLevelSucceeded()) {
      this.context.runLater(() => {
        if (this.onLevelCompleted) {

          const stars = this.cheeseList.length % 4

          this.onLevelCompleted({
            stars: stars as 1 | 2 | 3,
            score: this.score
          })
        }
      })

      return true
    }

    return false
  }

  private isLevelSucceeded () {
    return this.gameTimer.expired
  }

  private isLevelFailed () {
    return this.remainingCheeseCount <= 0
  }

  private addCat (x, y) {
    const cat = new Cat(x, y)
    cat.onFire = (params) => this.handleCatFire(params)
    this.add(cat)
    this.theCat = cat
  }

  private createCheese (x, y) {
    const cheese = new Cheese(x, y)
    this.add(cheese)
  }

  private removeLater (entity: fw.Entity) {
    this.context.runLater(
      () => {
        this.remove(entity)
      })
  }

  private removeFromArray<T> (array: T[], entity: T) {
    let index: number
    if ((index = array.indexOf(entity)) >= 0) {
      array.splice(index, 1)
    }
  }
}
