import { Cheese, Hole } from 'static-entities'
import { Cat } from 'cat'

export interface LevelInfo {
  cheeses: Cheese[]
  holes: Hole[]
  cat: Cat
  addCheese (cheese: Cheese): void
  removeCheese (cheese: Cheese): void
}
