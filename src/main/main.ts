import { StateMachine } from 'state-machine/state'
import { MainMenuState } from './main-menu-state'
import { Game } from 'game'
import * as Levels from 'levels'
import { GameStorage } from 'storage'

class GameState {

  onAllLevelCompleted: () => void
  onExit: () => void

  constructor (private readonly game: Game, private readonly continueSaved: boolean) {}

  enter () {
    this.showHide('.exit-to-menu.button', true)
    this.exitButton().onclick = this.onExit

    this.game.onSucceeded = this.onAllLevelCompleted
    this.game.onExit = this.onExit

    if (this.continueSaved) {
      this.game.continueGame()
    } else {
      this.game.startNewGame()
    }
  }

  exit () {
    this.game.onSucceeded = null
    this.game.onExit = null
    this.game.destroy()

    this.showHide('.exit-to-menu.button', true)
    this.exitButton().onclick = null
  }

  private exitButton (): HTMLButtonElement {
    return document.querySelector('.exit-to-menu.button')
  }

  private showHide (selector: string, show: boolean) {
    (<HTMLElement>document.querySelector(selector)).style.display = show ? 'block' : 'none'
  }
}

export class Main {

  private readonly canvas: HTMLCanvasElement
  private readonly stateMachine: StateMachine
  private readonly gameStorage: GameStorage

  constructor () {
    this.canvas = document.getElementById('canvas') as HTMLCanvasElement
    this.stateMachine = new StateMachine()

    this.gameStorage = new GameStorage()
  }

  start () {

    this.menu()
  }

  menu () {
    const menu = new MainMenuState(this.gameStorage)
    menu.onStartClicked = () => {
      this.startGame(false)
    }
    menu.onContinueClicked = () => {
      this.startGame(true)
    }

    this.stateMachine.changeState(menu)
  }

  private startGame (continueSaved: boolean) {

    const game: Game = new Game(this.canvas, Levels.levels, this.gameStorage)
    const state = new GameState(game, continueSaved)
    state.onExit = () => {
      this.menu()
    }

    this.stateMachine.changeState(state)
  }
}
