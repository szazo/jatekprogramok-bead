import { GameStorage } from 'storage'

export class MainMenuState {

  onStartClicked: () => void
  onContinueClicked: () => void

  constructor (private readonly gameStorage: GameStorage) {
  }

  enter () {
    this.showHide('.main-menu', true)
    this.showHide('.main-menu .continue-game-container', this.gameStorage.hasSavedGame)
    this.showHighscore()

    this.newGameButton().onclick = () => { this.onStartClicked() }
    this.continueGameButton().onclick = () => { this.onContinueClicked() }
    this.helpButton().onclick = () => { this.showHelp() }
    this.closeHelpButton().onclick = () => { this.closeHelp() }
  }

  exit () {
    this.newGameButton().onclick = null
    this.continueGameButton().onclick = null
    this.helpButton().onclick = null
    this.closeHelpButton().onclick = null
    this.showHide('.main-menu', false)
  }

  private showHighscore () {
    if (this.gameStorage.hasHighscore) {
      document.querySelector('.main-menu .highscore').textContent = 'Highscore: ' + this.gameStorage.highScore
    }
    this.showHide('.main-menu .highscore', this.gameStorage.hasHighscore)
  }

  private newGameButton (): HTMLButtonElement {
    return (document.querySelector('#new-game'))
  }

  private continueGameButton (): HTMLButtonElement {
    return (document.querySelector('#continue-game'))
  }

  private helpButton (): HTMLButtonElement {
    return (document.querySelector('#help'))
  }

  private closeHelpButton (): HTMLButtonElement {
    return (document.querySelector('#close-help'))
  }

  private showHelp () {
    this.showHide('.help.window', true)
  }

  private closeHelp () {
    this.showHide('.help.window', false)
  }

  private showHide (selector: string, show: boolean) {
    (<HTMLElement>(document.querySelector(selector))).style.display = show ? 'block' : 'none'
  }
}
