import * as Maps from './maps'
import { LevelDescriptor } from 'level/level-descriptor'

export const levels: LevelDescriptor[] = [
  {
    map: Maps.map0,
    gameSeconds: 30,
    mouseIntervalMs: { min: 1500, max: 2000 },
    bulletCount: 100
  },
  {
    map: Maps.map1,
    gameSeconds: 45,
    mouseIntervalMs: { min: 1200, max: 2000 },
    bulletCount: 120
  },
  {
    map: Maps.map2,
    gameSeconds: 45,
    mouseIntervalMs: { min: 1200, max: 2000 },
    bulletCount: 130
  },
  {
    map: Maps.map3,
    gameSeconds: 45,
    mouseIntervalMs: { min: 1200, max: 2000 },
    bulletCount: 150
  },
  {
    map: Maps.map4,
    gameSeconds: 45,
    mouseIntervalMs: { min: 1200, max: 3000 },
    bulletCount: 150
  },
  {
    map: Maps.map5,
    gameSeconds: 45,
    mouseIntervalMs: { min: 1200, max: 2000 },
    bulletCount: 150
  },
  {
    map: Maps.map6,
    gameSeconds: 45,
    mouseIntervalMs: { min: 1000, max: 2000 },
    bulletCount: 150
  },
  {
    map: Maps.map6,
    gameSeconds: 45,
    mouseIntervalMs: { min: 800, max: 1500 },
    bulletCount: 150
  }
]
