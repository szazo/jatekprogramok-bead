import { UpdateParams } from 'events'
import { State } from 'state-machine';

export interface CatState extends State {
  update (params: UpdateParams): void
}
