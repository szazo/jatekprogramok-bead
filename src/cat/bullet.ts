import { Context } from 'game'
import { PTM } from 'common'
import { EntityCategories } from 'level'
import { UpdateParams } from 'events';

interface Vector {
  x: number
  y: number
}

export class Bullet extends fw.EntityWithBody {

  static get BOUNDING_BOX () {
    return { left: 0, top: 0, width: 11, height: 11 }
  }

  public static sprite = fw.image('./images/wool_ball18x12.png')
  public static events = [ 'add', 'remove', 'contact', 'update' ]
  private static readonly bodyDef: planck.BodyDef = {
    type: 'dynamic'
  }
  private static readonly fixtureDef: planck.FixtureDefWithShape = {
    shape: fw.Box(11 / PTM, 11 / PTM),
    restitution: 0.6,
    friction: 1,
    density: 0.15,
    filterCategoryBits: EntityCategories.bullet,
    filterMaskBits: EntityCategories.mouse | EntityCategories.wall
  }

  public onVanish: () => void

  private hp: number
  private birth: number

  constructor (x: number, y: number,
              private readonly direction: Vector,
              private readonly context: Context) {
    super(x, y)
    this.image = Bullet.sprite
    this.hp = 1.0
    this.birth = 0
  }

  add () {

    const tnx = this.direction.y / this.direction.x
    const degree = Math.atan(tnx)

    this.body = this.context.world.createBody(Bullet.bodyDef)
    this.body.createFixture(Bullet.fixtureDef)
    this.body.setPosition(planck.Vec2(this.x / PTM, this.y / PTM))
    this.body.setLinearVelocity(planck.Vec2(this.direction.x * 10, this.direction.y * 10))
    this.body.setUserData(this)
    this.body.setAngle(degree)
  }

  remove () {
    this.context.world.destroyBody(this.body)
  }

  contact (other: { entity: fw.Entity }) {

    if (other.entity instanceof Bullet) {
      return
    }

    this.hp = this.hp - 0.3
  }

  update (params: UpdateParams) {

    if (!this.birth) { this.birth = params.time }
    const live = params.time - this.birth
    if (live > 1000) {
      this.hp = 0
    }

    if (this.hp <= 0.001) {
      this.hp = 0
      if (this.onVanish) {
        this.onVanish()
      }
    }

    this.alpha = this.hp
  }

  getTop () {
    return this.y + Bullet.BOUNDING_BOX.top
  }

  getLeft () {
    return this.x + Bullet.BOUNDING_BOX.left
  }

  getWidth () {
    return Bullet.BOUNDING_BOX.width
  }

  getHeight () {
    return Bullet.BOUNDING_BOX.height
  }
}
