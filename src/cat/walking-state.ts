import { UpdateParams } from 'events'
import { Wall } from 'static-entities'
import { SpriteAnimation, AnimationDescriptor } from 'sprite';
import { DistanceCalculator, VectorMovement, Position, Direction, KeyboardMovement } from 'common';
import { CatState } from './cat-state'
import { Cat } from './cat'

export class WalkingState implements CatState {

  onStopped: (direction: Direction) => void
  
  private keyboardMovement: KeyboardMovement
  private distanceCalculator: DistanceCalculator
  private animation: SpriteAnimation

  constructor(private direction: Direction,
              private owner: Cat,
              animation: AnimationDescriptor[]) {

    this.animation = new SpriteAnimation(animation)
    this.keyboardMovement = new KeyboardMovement()
    const CAT_SPEED_TILE_SEC = 2
    this.distanceCalculator = new DistanceCalculator(CAT_SPEED_TILE_SEC)
  }

  update (params: UpdateParams) {

    const newDirection = this.keyboardMovement.direction()
    const vector = this.keyboardMovement.vector()
    if (newDirection === Direction.none) {
      this.onStopped(this.direction)
      return
    }

    if (!this.animation.isStarted || this.direction !== newDirection) {
      this.animation.start(newDirection)
    }

    // movement
    this.direction = newDirection

    const distanceToGo = this.distanceCalculator.advance(params.time)
    const delta = VectorMovement.calculate(vector, distanceToGo)

    const target = {
      x: this.owner.x + delta.x,
      y: this.owner.y + delta.y
    }

    if (!this.checkWall(target, params)) {
      this.owner.x += delta.x
      this.owner.y += delta.y
    }
    
    // animation
    this.animation.update(params.time)
    let offset = this.animation.currentOffset()
    if (!offset) {
      return
    }

    this.owner.updateSprite(this.direction, offset)
  }

  private checkWall(target: Position, params: UpdateParams) {

    const collided = params.index.query(Cat.boundingBoxLeft(target.x),
                               Cat.boundingBoxTop(target.y),
                               Cat.boundingBoxWidth(Cat.WIDTH),
                               Cat.boundingBoxHeight(Cat.HEIGHT))
    const found = collided.length > 0 &&
          collided.find((entity: fw.Entity) => entity instanceof Wall)
    return found
  }
}
