import { UpdateParams } from 'events'
import { SpriteSheet } from 'sprite';
import { Direction, KeyboardMovement } from 'common';
import { CatState } from './cat-state'
import { Cat } from './cat'

export class StandingState implements CatState {

  onMovementStarted: (direction: Direction) => void

  private keyboardMovement: KeyboardMovement

  constructor(private direction: Direction,
              private owner: Cat,
              private spriteSheet: SpriteSheet) {
    this.keyboardMovement = new KeyboardMovement()
  }

  update (_params: UpdateParams) {
    const direction = this.keyboardMovement.direction()
    if (direction !== Direction.none) {
      this.onMovementStarted(direction)
      return
    }

    let sprite = this.spriteSheet.find(this.direction)
    this.owner.updateSprite(this.direction, { x: sprite.startX, y: sprite.startY })
  }
}

