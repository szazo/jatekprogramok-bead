import { Direction, IntervalTimer, DirectionToVector } from 'common';
import { Cat } from './cat'

export interface FireParams {
  x: number,
  y: number,
  direction: { x: number, y: number }
}

export class CatFire {

  private timer: IntervalTimer

  constructor(private owner: Cat) {
    const FIRE_INTERVAL = 200
    this.timer = new IntervalTimer(FIRE_INTERVAL, { fireOnFirst: true })
  }

  onFire: (params: FireParams) => void

  update(time: number, direction: Direction) {

    if (!fw.isDown(32)) {
      return
    }

    if (this.timer.check(time)) {
      this.onFire({
        x: this.owner.x + 10,
        y: this.owner.y + 10,
        direction: DirectionToVector.calculate(direction)
      })
    }
  }
}
