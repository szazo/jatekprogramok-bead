import { UpdateParams } from 'events'
import { StateMachine } from 'state-machine';
import { AnimationDescriptor, SpriteSheet, Sprite } from 'sprite';
import { Position, Direction } from 'common';
import { CatState } from './cat-state'
import { StandingState } from './standing-state'
import { WalkingState } from './walking-state'
import { CatFire, FireParams } from './cat-fire'

export class Cat extends fw.Entity {

  public onFire: (params: FireParams) => void

  private sm: StateMachine<CatState>
  private catFire: CatFire

  private currentSpriteOffset: Position = { x: 0, y: 0 }
  private currentDirection: Direction = Direction.none

  constructor (x: number,
              y: number) {
    super(x, y)
    this.sm = new StateMachine<CatState>()
    this.catFire = new CatFire(this)
    this.catFire.onFire = (params) => this.onFire(params)

    this.stand(Direction.up)
  }

  private stand(direction: Direction) {
    const state = new StandingState(direction, this, Cat.spriteSheet)
    state.onMovementStarted = (direction) => {
      this.move(direction)
    }

    this.sm.changeState(state)
  }

  private move(direction: Direction) {
    const state = new WalkingState(direction, this, Cat.spriteAnimation)
    state.onStopped = (direction) => {
      this.stand(direction)
    }

    this.sm.changeState(state)
  }

  static calculateBoundingBox (x: number, y: number) {
    return {
      left: Cat.boundingBoxLeft(x),
      top: Cat.boundingBoxTop(y),
      width: this.boundingBoxWidth(Cat.WIDTH),
      height: this.boundingBoxHeight(Cat.HEIGHT)
    }
  }

  static boundingBoxLeft (x: number) {
    return x + 6
  }

  static boundingBoxTop (y: number) {
    return y + 12
  }

  static boundingBoxWidth (width: number) {
    return width - 12
  }

  static boundingBoxHeight (height: number) {
    return height - 12
  }

  update (params: UpdateParams) {

    this.sm.executeAction((state) => state.update(params))
    this.catFire.update(params.time, this.currentDirection)
  }

  updateSprite(direction: Direction, offset: Position) {
    this.currentDirection = direction
    this.currentSpriteOffset = offset
  }

  static TILE_SIZE = 32
  static get WIDTH () { return Cat.TILE_SIZE }
  static get HEIGHT () { return Cat.TILE_SIZE }

  get position () {
    return { x: this.x, y: this.y }
  }

  draw (ctx: CanvasRenderingContext2D) {

    ctx.drawImage(Cat.sprite,
            this.currentSpriteOffset.x, this.currentSpriteOffset.y, Cat.TILE_SIZE, Cat.TILE_SIZE,
                  this.x, this.y, Cat.TILE_SIZE, Cat.TILE_SIZE)
  }

  getLeft () {
    return Cat.boundingBoxLeft(this.x)
  }

  getTop () {
    return Cat.boundingBoxTop(this.y)
  }

  getWidth () {
    return Cat.boundingBoxWidth(Cat.WIDTH)
  }

  getHeight () {
    return Cat.boundingBoxHeight(Cat.HEIGHT)
  }

  private static spriteSheet = new SpriteSheet([
    new Sprite(Direction.down, 0, 1),
    new Sprite(Direction.left, 0, Cat.TILE_SIZE + 1),
    new Sprite(Direction.right, 0, 2 * Cat.TILE_SIZE + 1),
    new Sprite(Direction.up, 0, 3 * Cat.TILE_SIZE + 1),

    new Sprite(Direction.downLeft, 3 * Cat.TILE_SIZE, 0 + 1),
    new Sprite(Direction.upLeft, 3 * Cat.TILE_SIZE, Cat.TILE_SIZE + 1),
    new Sprite(Direction.downRight, 3 * Cat.TILE_SIZE, 2 * Cat.TILE_SIZE + 1),
    new Sprite(Direction.upRight, 3 * Cat.TILE_SIZE, 3 * Cat.TILE_SIZE + 1)
  ])

  private static readonly ANIM_LENGTH = 3
  private static spriteAnimation = [
    AnimationDescriptor.fromSprite(Direction.down,
                                   Cat.spriteSheet.find(Direction.down), Cat.TILE_SIZE, 0, Cat.ANIM_LENGTH),
    AnimationDescriptor.fromSprite(Direction.left,
                                   Cat.spriteSheet.find(Direction.left), Cat.TILE_SIZE, 0, Cat.ANIM_LENGTH),
    AnimationDescriptor.fromSprite(Direction.right,
                                   Cat.spriteSheet.find(Direction.right), Cat.TILE_SIZE, 0, Cat.ANIM_LENGTH),
    AnimationDescriptor.fromSprite(Direction.up,
                                   Cat.spriteSheet.find(Direction.up), Cat.TILE_SIZE, 0, Cat.ANIM_LENGTH),
    
    AnimationDescriptor.fromSprite(Direction.downLeft,
                                   Cat.spriteSheet.find(Direction.downLeft), Cat.TILE_SIZE, 0, Cat.ANIM_LENGTH),
    AnimationDescriptor.fromSprite(Direction.upLeft,
                                   Cat.spriteSheet.find(Direction.upLeft), Cat.TILE_SIZE, 0, Cat.ANIM_LENGTH),
    AnimationDescriptor.fromSprite(Direction.downRight,
                                   Cat.spriteSheet.find(Direction.downRight), Cat.TILE_SIZE, 0, Cat.ANIM_LENGTH),
    AnimationDescriptor.fromSprite(Direction.upRight,
                                   Cat.spriteSheet.find(Direction.upRight), Cat.TILE_SIZE, 0, Cat.ANIM_LENGTH),
  ]

  public static readonly sprite = fw.image('./images/cat.png')
  public static events = [ 'update', 'draw' ]
}
