import { Sprite } from "./sprite-sheet";

export class AnimationDescriptor {

  constructor (
    public readonly name: string,
    public readonly startX: number,
    public readonly startY: number,
    public readonly offsetX: number,
    public readonly offsetY: number,
    public readonly length: number) {

  }

  static fromSprite(name: string,
                    sprite: Sprite,
                    offsetX: number,
                    offsetY: number,
                    length: number) {

    return new AnimationDescriptor(name,
                                   sprite.startX,
                                   sprite.startY,
                                   offsetX,
                                   offsetY,
                                   length)
  }

  calculate (anim: number) {
    const mod = anim % this.length
    const x = this.startX + (mod * this.offsetX)
    const y = this.startY + (mod * this.offsetY)

    return { x, y }
  }
}

export class SpriteAnimation {

  private isMoving: boolean
  private startTime: number
  private anim: number
  private currentAnimation: AnimationDescriptor | null

  constructor (private readonly animations: AnimationDescriptor[]) {

    this.isMoving = false
    this.startTime = 0
    this.anim = 0
    this.currentAnimation = null
  }

  start (name: string) {
    const animation = this.animations.find((x) => x.name === name)
    if (!animation) { throw new Error(`Animation ${name} not found`) }

    this.currentAnimation = animation
    this.isMoving = true
    this.startTime = 0
  }

  stop () {
    this.isMoving = false
    this.startTime = 0
    this.currentAnimation = null
  }

  update (time: number) {
    if (!this.isMoving) {
      return
    }

    if (!this.startTime) {
      // first
      this.startTime = time
      return
    }

    const delta = time - this.startTime
    this.anim = Math.floor(delta / 100)
  }

  currentOffset () {
    if (!this.isMoving) { return false }

    return this.currentAnimation.calculate(this.anim)
  }

  get isStarted() {
    return this.isMoving
  }
}
