
export class Sprite {

  constructor (
    public readonly name: string,
    public readonly startX: number,
    public readonly startY: number) {

  }
}

export class SpriteSheet {

  constructor (private readonly sprites: Sprite[]) {
  }

  find (name: string): Sprite | null {
    const sprite = this.sprites.find((x) => x.name === name)

    return sprite
  }
}
