import { UpdateEvent, UpdateParams } from 'events/update-event'

export interface State {
  enter? ()
  exit? ()
}

export abstract class BaseState implements State {

  public enter () {}
  public exit () {}
}

export class StateMachine<TState extends State = State> {

  private currentState: TState

  changeState (state: TState) {

    if (this.currentState && this.currentState.exit) {
      this.currentState.exit()
    }

    this.currentState = state

    if (state.enter) {
      state.enter()
    }
  }

  executeAction (action: (state: TState) => void) {
    action(this.currentState)
  }
}
