import { Cheese, Wall } from 'static-entities'
import { AnimationDescriptor, SpriteAnimation } from 'sprite'
import { Context } from 'game'
import { Bullet } from 'cat'
import { Position, PTM, Direction, VectorToDirection } from 'common'
import { UpdateEvent, UpdateParams } from 'events/update-event'
import { LevelInfo } from 'level'
import { StateMachine } from 'state-machine'
import { FindCheeseState } from './find-cheese-state'
import { StealCheeseState } from './steal-cheese-state'
import { CollidedState } from './collided-state'
import { EntityCategories } from 'level'
import { NoCheeseGoBackState } from './no-cheese-go-back-state'
import { FinishedState } from './finished-state'
import { MouseState } from './mouse-state'

export class Mouse extends fw.EntityWithBody implements UpdateEvent {


  public onDead: () => void
  public onHidden: (withCheese: boolean) => void

  private readonly sm: StateMachine<MouseState>
  private animationDirection: Direction

  constructor (
    x: number,
    y: number,
    private readonly levelInfo: LevelInfo,
    private readonly context: Context) {

    super(x, y)

    this.sm = new StateMachine<MouseState>()
  }

  get position (): Position {
    const bodyPos = this.body.getPosition()
    return { x: bodyPos.x * PTM, y: bodyPos.y * PTM }
  }

  changeAlpha (alpha: number) {
    this.alpha = alpha
  }

  add () {
    this.body = this.context.world.createBody(Mouse.bodyDef)
    this.body.createFixture(Mouse.fixtureDef)
    this.body.setPosition(planck.Vec2(this.x / PTM, this.y / PTM))
    this.body.setUserData(this)

    this.ensureAnimation(Direction.down)
    this.findCheese()
  }

  remove () {
    this.context.world.destroyBody(this.body)
  }

  moveTo (position: Position) {

    let direction = VectorToDirection.calculate(this.position, position)
    this.ensureAnimation(direction)

    this.body.setPosition(planck.Vec2(
      position.x / PTM,
      position.y / PTM))
  }

  private ensureAnimation(direction: Direction) {
    if (!this.animation.isStarted || (direction !== Direction.none && direction !== this.animationDirection)) {
      this.animationDirection = direction
      this.animation.start(direction)
    }
  }

  update (params: UpdateParams) {

    this.sm.executeAction((state) => state.update(params))
    this.animation.update(params.time)
  }

  contact (other: { entity: fw.Entity }) {

    if (other.entity instanceof Bullet) {
      this.sm.executeAction((state) => {
        if (state.contact) {
          state.contact(other.entity)
        }
      })
    }
  }

  calculateBoundingBox (x: number, y: number) {
    return {
      left: x,
      top: y,
      width: this.getWidth(),
      height: this.getHeight()
    }
  }

  checkWall (index: fw.Index, target: fw.Entity) {
    const collided = index.query(target.x,
                               target.y,
                               this.getWidth(),
                               this.getHeight())

    const found = collided.length > 0 &&
        collided.find((entity) => entity instanceof Wall)
    if (found) {
      return true
    }

    return false
  }

  private findCheese () {

    const state = new FindCheeseState(this, this.levelInfo)
    state.onCheeseReached = (cheese: Cheese) => {
      this.stealCheese(cheese)
    }
    state.onCheeseNotFound = () => {
      this.noCheeseGoBack()
    }
    state.onContacted = () => {
      this.collided()
    }

    this.sm.changeState(state)
  }

  private stealCheese (cheese: Cheese) {
    const state = new StealCheeseState(this, cheese, this.levelInfo)
    state.onHoleReached = () => {
      this.finished()
      this.onHidden(true)
    }
    state.onContacted = () => {
      this.collided()
    }

    this.sm.changeState(state)
  }

  private noCheeseGoBack () {
    const state = new NoCheeseGoBackState(this, this.levelInfo)
    state.onCheeseFound = () => {
      // cheese found while going back to the hole
      this.findCheese()
    }
    state.onContacted = () => {
      this.collided()
    }
    state.onHoleReached = () => {
      this.finished()
      this.onHidden(false)
    }

    this.sm.changeState(state)
  }

  private collided () {
    const state = new CollidedState(this)
    state.onDead = () => {
      this.finished()
      this.onDead()
    }
    this.sm.changeState(state)
  }

  private finished () {
    this.sm.changeState(new FinishedState())
  }

  draw (ctx: CanvasRenderingContext2D) {

    const offset = this.animation.currentOffset()
    if (offset === false) {
      return
    }

    //this.drawPolygon(ctx)
    //this.drawRectangle(ctx)
    ctx.globalAlpha = this.alpha
    ctx.drawImage(Mouse.sprite,
            offset.x, offset.y,  Mouse.TILE_SIZE, Mouse.TILE_SIZE,
                  this.position.x, this.position.y + Mouse.VOFFSET, Mouse.WIDTH, Mouse.HEIGHT)
    ctx.globalAlpha = 1
  }

  // private drawRectangle (ctx: CanvasRenderingContext2D) {
  //   ctx.beginPath()
  //   ctx.lineWidth = 1
  //   ctx.strokeStyle = 'red'
  //   ctx.rect(this.getLeft(), this.getTop(), this.getWidth(), this.getHeight())
  //   ctx.stroke()
  // }

  private static readonly bodyDef: planck.BodyDef = {
    type: 'dynamic',
    fixedRotation: true
  }

  private static readonly WIDTH = 25
  private static readonly HEIGHT = 25
  private static readonly VOFFSET = -10
  
  private static readonly fixtureDef: planck.FixtureDefWithShape = {
    shape: fw.Box(Mouse.WIDTH / PTM, (Mouse.HEIGHT + Mouse.VOFFSET) / PTM),
    restitution: 0.5,
    friction: 0.5,
    density: 0.5,
    filterCategoryBits: EntityCategories.mouse,
    filterMaskBits: EntityCategories.cat | EntityCategories.wall | EntityCategories.bullet
  }

  static get TILE_SIZE () { return 48 }
  static readonly events = [ 'add', 'remove', 'update', 'contact', 'draw' ]

  public static readonly sprite = fw.image('./images/mouse.png')

  private static readonly ANIM_LENGTH = 4
  private readonly animation: SpriteAnimation = new SpriteAnimation([
    new AnimationDescriptor(Direction.down, 0, 0, Mouse.TILE_SIZE, 0, Mouse.ANIM_LENGTH),
    new AnimationDescriptor(Direction.left, 0, Mouse.TILE_SIZE, Mouse.TILE_SIZE, 0, Mouse.ANIM_LENGTH),
    new AnimationDescriptor(Direction.upLeft, 0, Mouse.TILE_SIZE, Mouse.TILE_SIZE, 0, Mouse.ANIM_LENGTH),
    new AnimationDescriptor(Direction.downLeft, 0, Mouse.TILE_SIZE, Mouse.TILE_SIZE, 0, Mouse.ANIM_LENGTH),
    new AnimationDescriptor(Direction.right, 0, 2 * Mouse.TILE_SIZE, Mouse.TILE_SIZE, 0, Mouse.ANIM_LENGTH),
    new AnimationDescriptor(Direction.upRight, 0, 2 * Mouse.TILE_SIZE, Mouse.TILE_SIZE, 0, Mouse.ANIM_LENGTH),
    new AnimationDescriptor(Direction.downRight, 0, 2 * Mouse.TILE_SIZE, Mouse.TILE_SIZE, 0, Mouse.ANIM_LENGTH),
    new AnimationDescriptor(Direction.up, 0, 3 * Mouse.TILE_SIZE, Mouse.TILE_SIZE, 0, Mouse.ANIM_LENGTH)
  ])
}
