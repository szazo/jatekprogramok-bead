import { Mouse } from './mouse'
import { Cheese } from 'static-entities'
import { IntervalTimer } from 'common'
import { UpdateParams } from 'events'
import { LevelInfo } from 'level'
import { Router } from './router'

export class StealCheeseState {

  public onHoleReached: () => void
  public onContacted: () => void
  private readonly rerouteTimer: IntervalTimer
  private readonly router: Router

  constructor (
    private readonly owner: Mouse,
    private readonly cheese: Cheese,
    private readonly levelInfo: LevelInfo) {

    const REROUTE_TIME = 500
    this.rerouteTimer = new IntervalTimer(REROUTE_TIME, { fireOnFirst: true })
    this.router = new Router(owner, levelInfo)
  }

  public contact () {

    this.cheese.setPosition(this.owner.position)
    this.cheese.lost()
    this.levelInfo.addCheese(this.cheese)

    this.onContacted()
  }

  public update (params: UpdateParams) {

    if (this.router.isReached) {
      this.onHoleReached()
      return
    }

    const intervalFired = this.rerouteTimer.check(params.time)
    const needReroute = intervalFired &&
          (!this.router.hasRoute || !this.router.checkPath(params.index))

    if (needReroute) {
      this.router.routeToNearest(this.levelInfo.holes, params.index)
    }

    if (this.router.hasRoute) {
      this.router.advance(params.time)
    }
  }
}
