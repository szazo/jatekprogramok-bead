import { UpdateParams } from 'events'
import { IntervalTimer } from 'common'
import { Mouse } from './mouse'

export class CollidedState {

  public onDead: () => void

  private startTime: number
  private readonly flashTimer: IntervalTimer
  private state = 0

  constructor (private readonly owner: Mouse) {

    const FLASH_TIME = 100
    this.flashTimer = new IntervalTimer(FLASH_TIME, { fireOnFirst: false })
  }

  public update (params: UpdateParams) {

    if (!this.startTime) {
      this.startTime = params.time
    }

    if (params.time - this.startTime > 1000) {
      this.onDead()
      return
    }

    if (this.flashTimer.check(params.time)) {
      this.state = (this.state + 1) % 2
      this.owner.changeAlpha(this.state === 0 ? 0.1 : 1)
    }
  }
}
