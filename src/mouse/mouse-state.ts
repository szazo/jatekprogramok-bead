import { State } from 'state-machine'
import { UpdateParams } from 'events/update-event'

export interface MouseState extends State {
  update (params: UpdateParams): void
  contact? (entity: fw.Entity): void
}
