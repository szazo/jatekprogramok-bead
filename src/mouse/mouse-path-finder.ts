import { NodeProvider, PathFinder } from 'common/path-finder'
import { Coord, Position } from 'common'

export class MouseNodeProvider implements NodeProvider {

  static directions: [number, number][] = [
    [ 0, -1 ], [ 1, -1 ], [ 1, 0 ], [ 1, 1 ], [ 0, 1 ], [ -1, 1 ], [ -1, 0 ], [ -1, -1 ]
  ]

  constructor (private readonly isValidPosition: (position: Position) => boolean,
              private readonly step: number) {
  }

  provide (coord: Coord) {
    const list: Coord[] = []

    for (const direction of MouseNodeProvider.directions) {

      const targetX = coord.x + direction[0] * this.step
      const targetY = coord.y + direction[1] * this.step

      if (!this.isValidPosition({ x: targetX, y: targetY })) {
        continue
      }

      list.push(new Coord(targetX, targetY))
    }

    return list
  }
}

export class MousePathFinder {

  constructor (
    private readonly step: number) {
  }

  findPath (
    position: Position,
    goal: Position,
    isValidPosition: (position: Position) => boolean) {

    const nodeProvider = new MouseNodeProvider(isValidPosition, this.step)
    const pathFinder = new PathFinder(nodeProvider)
    const path = pathFinder.calculate(new Coord(position.x, position.y),
                                         new Coord(goal.x, goal.y))

    return path
  }
}
