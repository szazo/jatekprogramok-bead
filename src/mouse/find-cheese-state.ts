import { UpdateParams } from 'events'
import { Mouse } from './mouse'
import { Cheese } from 'static-entities'
import { LevelInfo } from 'level'
import { IntervalTimer } from 'common'
import { Router } from './router'

export class FindCheeseState {
  public onCheeseReached: (cheese: Cheese) => void
  public onCheeseNotFound: () => void
  public onContacted: () => void

  private cheese: Cheese | null
  private readonly rerouteTimer: IntervalTimer
  private readonly router: Router

  constructor (
    owner: Mouse,
    private readonly levelInfo: LevelInfo) {

    const REROUTE_TIME = 500
    this.rerouteTimer = new IntervalTimer(REROUTE_TIME, { fireOnFirst: true })
    this.router = new Router(owner, levelInfo)
  }

  public contact () {

    this.onContacted()
  }

  public update (params: UpdateParams) {

    if (this.router.isReached) {
      if (this.isCurrentCheeseFree) {
        this.cheese.stole()
        this.levelInfo.removeCheese(this.cheese)

        this.onCheeseReached(this.cheese)
        return
      } else {
        if (!this.cheese) {
          this.onCheeseNotFound()
          return
        }
      }
    }

    const intervalFired = this.rerouteTimer.check(params.time)
    // we only reroute in specific time interval
    const needReroute = intervalFired &&  (!this.isCurrentCheeseFree() ||
                                         (!this.router.hasRoute || !this.router.checkPath(params.index)))

    if (needReroute) {
      const freeCheeses = this.levelInfo.cheeses.filter((x) => !x.isStolen)
      this.cheese = this.router.routeToNearest(freeCheeses, params.index)

      if (!this.cheese) {
        this.onCheeseNotFound()
        return
      }
    }

    if (this.router.hasRoute) {
      this.router.advance(params.time)
    }
  }

  private isCurrentCheeseFree () {
    return this.cheese && !this.cheese.isStolen
  }
}
