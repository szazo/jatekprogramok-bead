import { Coord, findNearest, Position, WithPosition } from 'common'
import * as Common from 'common'
import { MousePathFinder } from './mouse-path-finder'
import { Mouse } from './mouse'
import { Cat } from 'cat'
import { Hole, Cheese } from 'static-entities'
import { LevelInfo } from 'level'

/**
 * Base class for route finding mouse
 */
export class Router {

  get hasRoute (): boolean {
    return !!this.pathAdvancer
  }

  get isReached (): boolean {
    return this.hasRoute && this.pathAdvancer.isReached
  }

  private readonly mousePathFinder: MousePathFinder
  private pathAdvancer: Common.PathAdvancer
  private distanceCalculator: Common.DistanceCalculator

  constructor (protected owner: Mouse,
               protected levelInfo: LevelInfo) {

    const STEP = 8 // the route point distances
    this.mousePathFinder = new MousePathFinder(STEP)
  }

  routeToNearest<T extends WithPosition> (targets: T[], index: fw.Index): T | null {

    const list = [ ...targets ]
    while (list.length > 0) {
      const item = findNearest(this.owner.position, list)
      if (this.reroute(item.position, index)) {
        return item
      }

      const i = list.indexOf(item)
      list.splice(i, 1)
    }

    this.clearRoute()

    return null
  }

  /**
   * Rechecks the current path whether according to the new index it is valid
   */
  checkPath (index: fw.Index) {

    if (!this.hasRoute) {
      return false
    }

    const MAX_STEP = 40
    let i = 0
    while (i < this.pathAdvancer.currentPath.length && i < MAX_STEP) {

      if (!this.isValidPosition(this.pathAdvancer.currentPath[i], index)) {
        return false
      }

      i++
    }

    return true
  }

  clearRoute () {
    this.pathAdvancer = null
    this.distanceCalculator = null
  }

  advance (time: number) {
    if (!this.hasRoute) {
      return false
    }

    // calculate the distance to go
    const distanceToGo = this.distanceCalculator.advance(time)

    const newPosition = this.pathAdvancer.advance(this.owner.position, distanceToGo)
    if (newPosition) {
      this.owner.moveTo(newPosition)
    }

    return true
  }

  protected reroute (goal: Position, index: fw.Index): boolean {

    const isValidPosition = (position) => {
      return this.isValidPosition(position, index)
    }
    const path = this.mousePathFinder.findPath(this.owner.position, goal, isValidPosition)

    if (path.length > 1) {
      path.shift() // the first is the source
      this.pathAdvancer = new Common.PathAdvancer(path)
      const MOUSE_SPEED = 3
      this.distanceCalculator = new Common.DistanceCalculator(MOUSE_SPEED)

      return true
    }

    this.clearRoute()

    return false
  }

  /**
   * This method called from the path finding algorithm to determine whether the
   * specified position can be used or not.
   */
  private isValidPosition (position: Position, index: fw.Index) {

    const CAT_RADIUS = 30
    const catCoord = Coord.fromPosition(this.levelInfo.cat.position)
    // if we are not in the cat radius, we take in to account while planning the path,
    // otherwise we cannot, because the path planning would fail
    if (Coord.fromPosition(this.owner.position).d(catCoord) > CAT_RADIUS) {

      if (Coord.fromPosition(position).d(catCoord) <= CAT_RADIUS) {
        return false
      }
    }

    let box =  this.owner.calculateBoundingBox(position.x, position.y)
    // add a margin for the box
    const MARGIN = 0
    box = { left: box.left - MARGIN,
      top: box.top - MARGIN,
      width: box.width + MARGIN * 2,
      height: box.height + MARGIN * 2 }

    const collided = index.query(box.left, box.top,
                               box.width, box.height)

    for (const item of collided) {
      if (!(item instanceof Mouse) && !(item instanceof Cheese)
          && !(item instanceof Hole) && !(item instanceof Cat)) {
        return false
      }
    }

    return true
  }
}
