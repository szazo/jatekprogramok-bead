import { Mouse } from './mouse'
import { IntervalTimer } from 'common'
import { UpdateParams } from 'events'
import { LevelInfo } from 'level'
import { Router } from './router'

export class NoCheeseGoBackState  {

  public onHoleReached: () => void
  public onContacted: () => void
  public onCheeseFound: () => void
  private readonly rerouteTimer: IntervalTimer
  private readonly router: Router

  constructor (
    owner: Mouse,
    private readonly levelInfo: LevelInfo) {

    const REROUTE_TIME = 500
    this.rerouteTimer = new IntervalTimer(REROUTE_TIME, { fireOnFirst: true })
    this.router = new Router(owner, levelInfo)
  }

  public contact () {

    this.onContacted()
  }

  public update (params: UpdateParams) {

    if (this.router.isReached) {
      this.onHoleReached()
      return
    }

    // let freeCheeses = this.levelInfo.cheeses.filter(x => !x.isStolen)
    // if (freeCheeses.length > 0) {
    //   this.onCheeseFound()
    //   return
    // }

    const intervalFired = this.rerouteTimer.check(params.time)
    const needReroute = intervalFired &&
          (!this.router.hasRoute || !this.router.checkPath(params.index))

    if (needReroute) {
      this.router.routeToNearest(this.levelInfo.holes, params.index)
    }

    if (this.router.hasRoute) {
      this.router.advance(params.time)
    }
  }
}
