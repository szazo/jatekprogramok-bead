export class Hole extends fw.EntityWithSprite {

  get position () {
    return { x: this.x, y: this.y }
  }

  public static readonly sprite = fw.image('./images/hole.png')

  constructor (x, y) {
    super(x, y)
    this.image = Hole.sprite
  }
}
