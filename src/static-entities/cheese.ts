import { Position } from 'common'

export class Cheese extends fw.EntityWithSprite {

  get isStolen () {
    return this.stolen
  }

  get position () {
    return { x: this.x, y: this.y }
  }

  public static readonly sprite = fw.image('./images/cheese.png')

  private stolen = false

  constructor (x, y) {
    super(x, y)
    this.image = Cheese.sprite
  }

  stole () {
    this.stolen = true
  }

  lost () {
    this.stolen = false
  }

  setPosition (position: Position) {
    this.x = position.x
    this.y = position.y
  }
}
