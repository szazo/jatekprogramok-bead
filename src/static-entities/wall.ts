import { PTM } from 'common'
import { Context } from 'game'
import { EntityCategories } from 'level'

export class Wall extends fw.EntityWithBody {
  static readonly events = [ 'add', 'remove' ]

  public static sprite = fw.image('./images/wall.png')
  static fixtureDef: planck.FixtureDefWithShape = {
    shape: fw.Box(32 / PTM, 32 / PTM),
    restitution: 0,
    friction: 1,
    density: 1,
    filterCategoryBits: EntityCategories.wall,
    filterMaskBits: EntityCategories.mouse | EntityCategories.bullet
  }
  static bodyDef: planck.BodyDef = {
    type: 'static'
  }

  private readonly world: planck.World

  constructor (x: number, y: number, context: Context) {
    super(x, y)

    this.world = context.world
    this.image = Wall.sprite
  }

  add () {

    this.body = this.world.createBody(Wall.bodyDef)
    this.body.createFixture(Wall.fixtureDef)
    this.body.setPosition(planck.Vec2(this.x / PTM, this.y / PTM))
  }

  remove () {
    this.world.destroyBody(this.body)
  }
}
