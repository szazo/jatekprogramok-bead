
declare namespace fw {

  type Image = any

  abstract class Entity {
    constructor(x: number, y: number)
    static events: string[]
    public x: number
    public y: number
    protected alpha: number
    public getLeft(): number
    public getTop(): number
    public getWidth(): number
    public getHeight(): number
  }
  
  abstract class EntityWithBody extends Entity {
    protected image: Image
    protected body: planck.Body
  }

  abstract class EntityWithSprite extends Entity {
    protected image: Image
  }

  class Scene {
    fireToEntity<T>(entity: Entity, name: string, params: T): void
    fire<T>(name: string, params: T): void
    entities: Entity[]
  }

  interface Index {
    query(left: number, top: number, width: number, height: number): Entity[]
  }

  function load(images: Image[], callback: () => void): void
  function isDown(keyCode: number): boolean
  function image(path: string): Image
  function createIndex(scene: Scene, size: number): Index
  function Box(width: number, height: number): planck.Shape
}

