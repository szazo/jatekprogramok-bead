# Cheese Protect Mission

Futó verzió: https://people.inf.elte.hu/fgsks8/jatek

## Fejlesztés

* Init: `yarn install`
* Fordítás: `yarn run build`
* Futtatás: `yarn run start:dev`, majd http://localhost:8080

## Használat

* Mozgás: kurzurbillentyűk
* Lövés: space

## Funkciók

* Játék
  * A macska tud lőni, a lövedék fizikai szimulációt tartalmaz
  * A gombolyag el tudja találni az egereket
  * Az egerek A* algoritmust használnak útkereséshez
  * Pontszámítás
  * Hátralévő gombolyagok száma
  * Hátralévő idő
  * Sikeres teljesítés esetén információs ablak (highscore esetén infó)
  * Sikertelen teljesítés esetén információs ablak, szint újrakezdése
  * Szint teljesítése után menüből folytatható a játék (LocalStorage)
  * Highscore kijelzés a főmenüben (LocalStorage)
  * 8 pályát tartalmaz
  * Minden szint teljesítése esetén gratuláció
* Egyéb:  
  * Főmenü
  * Súgó

## Állapotminta használata

* Macska:
  * StandingState
  * WalkingState

* Egér:
  * FindCheeseState (egy a lehető legközelebbi kiválasztott sajt felé megy)
    * Ha az adott sajtot már közben ellopta más egér, akkor próbál keresni újat
  * StealCheesestate (ellopja a sajtot, majd a lehető legközelebbi egérlyuk felé megy)
    * Ha közben lelövi a macska, elejti a sajtot
  * NoCheeseGoBackState (nincs elérhető sajt, visszamegy a lyukba)
    * Közben ellenőrzi, és ha talál sajtot, átvált FindCheeseState-be
  * CollidedState (lelőtték az egeret, flash, majd eltűnés)
  * FinishedState (az egér eltűnt)

* Játék:
  * LevelState: Egy szint folyamatban van
  * LevelCompletedState: Szint sikeres, megjelenik a sikeres ablak
  * LevelFailedState: Szint sikertelen, megjelenik a sikertelen ablak
  * AllLevelCompletedState: Minden szint sikeresen teljesítve

